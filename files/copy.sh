#!/bin/sh
sudo cp channels.scm /etc/guix/channels.scm
cp i3-config ~/.config/i3/config
cp gpg-agent.conf ~/.gpg-agent.conf
sudo cp inputrc /etc
cp gitconfig ~/.gitconfig
cp vimrc ~/.vimrc
cp sway.config ~/.config/sway/config
