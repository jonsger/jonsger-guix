;; This is an operating system configuration generated
;; by the graphical installer.

(use-modules (gnu)
             (nongnu packages linux)
             (nongnu system linux-initrd)
             (srfi srfi-1)) ;remove
(use-service-modules auditd base cups desktop dns mail mcron monitoring
                     networking sddm ssh syncthing sysctl virtualization xorg)
(use-package-modules cups printers  ; for customizing cups service
                     security-token ; nitrokey
                     wm)            ; comstomized screen locker

(operating-system
  (locale "de_DE.utf8")
  (timezone "Europe/Berlin")
  (keyboard-layout
    (keyboard-layout "de" "deadacute"))
  (host-name "3700X")
  (users (cons* (user-account
                  (name "jonathan")
                  (comment "Jonathan Brielmaier")
                  (group "users")
                  (home-directory "/home/jonathan")
                  (supplementary-groups
                    '("wheel" "netdev" "audio" "video" "kvm" "libvirt")))
                %base-user-accounts))
  (kernel linux)
  (initrd microcode-initrd)
  (firmware (list linux-firmware))
  (initrd-modules (append (list "realtek" "r8169") %base-initrd-modules))
  (sudoers-file (plain-file "sudoers"
                            "Defaults timestamp_timeout=45
                            root ALL=(ALL) ALL
                            %wheel ALL=(ALL) ALL"))
  (bootloader
    (bootloader-configuration
      (bootloader grub-efi-bootloader)
      (targets '("/boot/efi"))
      (keyboard-layout keyboard-layout)))
  (mapped-devices
    (list (mapped-device
            (source (uuid "87f38010-8f84-4d87-9c2f-abcfe47a472f"))
            (target "cryptroot")
            (type luks-device-mapping))))
  (file-systems
    (append (list
             (file-system
              (mount-point "/boot/efi")
              (device (uuid "C2BC-E7B1" 'fat32))
              (type "vfat"))
             (file-system
              (device "/dev/mapper/cryptroot")
              (mount-point "/")
              (type "ext4")
              (dependencies mapped-devices))
             (file-system
              (create-mount-point? #t)
              (device (uuid "bbf703ce-a3c2-4495-b9e8-0600a0c2dc1c"))
              (mount-point "/media/jonathan/datengrab")
              (type "xfs")))
            %base-file-systems))

  (packages
    (append
      (map (compose list specification->package+output)
       (list "audacity"
             "bind:utils" "borg" "bridge-utils"
             "calibre" "cryptsetup" "curl"
             "dmenu" "dmidecode"
             "evince"
             "ffmpeg" "file" "file-roller"
             "font-adobe-source-han-sans" "font-awesome" "font-dejavu" "font-google-noto" "font-hack" "fwupd-nonfree"
             "gdb" "gedit" "ghostscript" "gimp" "git" "git:send-email" "gnupg" "gnome-calculator" "gnome-maps" "gnome-system-monitor" "gnome-terminal" "grimshot"
             "gst-plugins-base" "gst-plugins-good"
             "htop" "hunspell-dict-de"
             "imagemagick" "inkscape"
             "josm"
             "keepassxc"
             "libreoffice" "lilypond" "lm-sensors" "lshw"
             "make" "mercurial" "mosh"
             "ncdu" "netcat" "network-manager-applet" "nheko" "nmap" "ntfs-3g"
             "okular" "openvpn"
             "pavucontrol" "patch" "pinentry" "poppler" "powertop" "python-future"
             "qemu" "qtwayland" ;"qgis"
             "rsync"
             "simple-scan" "smartmontools" "speedtest-cli" "strace" "sway" "swaylock" "system-config-printer"
             "ultrastar-deluxe" "unzip"
             "vim" "vim-airline" "vim-fugitive" "vim-guix-vim"
             "vlc"
             "telegram-desktop" "tigervnc-client" "transmission:gui" "tunctl"
             "vnstat"
             "waybar" "wget" "wireguard-tools" "wlr-randr"
             "xbacklight" "xdg-utils" "xfsprogs"
             "youtube-dl"
             "zstd"))
      %base-packages))

  (services
   (cons*
    (service cups-service-type
             (cups-configuration
              (web-interface? #t)
              (extensions
                (list cups-filters brlaser splix
                      (specification->package+output "splix:ppd"))))) ; brother, samsung
    (service gnome-desktop-service-type)
    (simple-service 'hosts
      hosts-service-type
        (list (host "144.76.7.123" "aemilia")
              (host "162.55.183.109" "baebia")
              (host "2a01:4f8:190:8242::1" "aemilia")
              (host "2a01:4f8:1c1c:b9df::1" "baebia")))
    (service libvirt-service-type
      (libvirt-configuration
        (unix-sock-group "libvirt")))
    (service screen-locker-service-type
      (screen-locker-configuration
        (name "swaylock")
        (program (file-append swaylock "/bin/swaylock"))))
    (service openssh-service-type
      (openssh-configuration
        (authorized-keys
          `(("jonathan" ,(local-file "keys/x270.pub"))))))
    (service sddm-service-type)
    (service syncthing-service-type
      (syncthing-configuration
        (user "jonathan")))
    ;(service static-networking-service-type
    ;  (list (static-networking
    ;    (addresses (list (network-address
    ;                       (device "eno1")
    ;                       (value "192.168.178.23/24"))))
    ;    (routes (list (network-route
    ;                    (destination "default")
    ;                    (gateway "192.168.178.1"))))
    ;    (name-servers '("9.9.9.9" "8.8.8.8")))))
    (service pam-limits-service-type
      (list
        (pam-limits-entry "*" 'both 'core 'unlimited)))
    (service qemu-binfmt-service-type
     (qemu-binfmt-configuration
       (platforms (lookup-qemu-platforms "arm" "aarch64" "ppc64le" "riscv64"))))
    (service vnstat-service-type)
    (service xfce-desktop-service-type)
    (udev-rules-service 'nitrokey libnitrokey)
    (remove
      (lambda (service)
        (let ((type (service-kind service)))
          (or (memq type
                    (list gdm-service-type))
;                          network-manager-service-type))
              (eq? 'network-manager-applet
                   (service-type-name type)))))
      (modify-services %desktop-services
        (guix-service-type config =>
          (guix-configuration
            (inherit config)
            (extra-options '("--cores=12"))
            (substitute-urls (append
                               %default-substitute-urls
                               (list "https://substitutes.nonguix.org")))
            (authorized-keys (append
                               %default-authorized-guix-keys
                               (list (local-file "keys/substitutes.nonguix.org.pub"))))))
        (sysctl-service-type config => config
          (sysctl-configuration
            (settings '(("kernel.core_pattern" . "/var/cores/core.%e.%p"))))))))))
