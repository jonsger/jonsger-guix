;; This is an operating system configuration generated
;; by the graphical installer.

(use-modules (gnu))
(use-service-modules certbot databases
                     mail mcron networking ssh web xorg)

(define %nginx-deploy-hook
            (program-file
             "nginx-deploy-hook"
             #~(let ((pid (call-with-input-file "/var/run/nginx/pid" read)))
                 (kill pid SIGHUP))))

(define backup-radicale-job
  ;; Backup radicale at 03:30 every day.
  #~(job "30 03 * * *"
         "bash /root/backup.sh"
         #:user "root"))

(operating-system
  (locale "de_DE.utf8")
  (timezone "Europe/Berlin")
  (keyboard-layout (keyboard-layout "de"))
  (host-name "baebia")
  (users (cons* (user-account
                  (name "jonathan")
                  (comment "Jonathan")
                  (group "users")
                  (home-directory "/home/jonathan")
                  (supplementary-groups
                    '("wheel" "netdev" "audio" "video")))
                %base-user-accounts))
  (bootloader
    (bootloader-configuration
      (bootloader grub-bootloader)
      (targets '("/dev/sda"))
      (keyboard-layout keyboard-layout)))
  (initrd-modules
    (append '("virtio_scsi") %base-initrd-modules))
  (swap-devices
    (list
      (swap-space
        (target (uuid "4d5e3c1e-179a-4f7c-aef8-659a07d424ee")))))
  (file-systems
    (cons* (file-system
             (mount-point "/")
             (device
               (uuid "a6f9bf76-3d53-4640-b17e-40e36cc9f6ae"
                     'ext4))
             (type "ext4"))
           %base-file-systems))

  (packages
    (append
      (map (compose list specification->package+output)
          (list
            "bind:utils" ; dig, nslookup
            "curl"
            "git"
            "gnupg"
            "htop"
            "iptables"
            "mariadb"
            "ncdu"
            "nmap"
            "php"
            "screen"
            "smartmontools"
            "unzip"
            "vim" "vim-airline" "vim-fugitive" "vim-guix-vim"
            "wget"
            "zstd"
            ; Backup
            "borg"
            "rsync"
            ; CalDAV/CardDAV
            "httpd" ; for httpasswd
            ))
            %base-packages))

  (services
    (cons*
      (service certbot-service-type
        (certbot-configuration
          (email "jonathan.brielmaier@web.de")
          (webroot "/srv/http")
          (certificates
          (list
            (certificate-configuration
              (domains '("brielmaier.net"))
              (deploy-hook %nginx-deploy-hook))))))
      (service dhcp-client-service-type)
      (service iptables-service-type
        (iptables-configuration
           (ipv4-rules (plain-file "iptables.rules" "*filter
-A INPUT -p tcp --dport 22 ! -s 127.0.0.1 -j REJECT
-A INPUT -p tcp --dport 3306 ! -s 127.0.0.1 -j REJECT
-A INPUT -p tcp --dport 5232 ! -s 127.0.0.1 -j REJECT
COMMIT
"))
           (ipv6-rules (plain-file "ip6tables.rules" "*filter
-A INPUT -p tcp --dport 22 ! -s ::1 -j REJECT
-A INPUT -p tcp --dport 3306 ! -s ::1 -j REJECT
-A INPUT -p tcp --dport 5232 ! -s ::1 -j REJECT
COMMIT
"))))
      (service mcron-service-type
         (mcron-configuration
           (jobs (list backup-radicale-job))))
      (service mysql-service-type)
      (service nginx-service-type
        (nginx-configuration
          (extra-content "    ssl_protocols TLSv1.2 TLSv1.3;
    ssl_ciphers ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384;")
          (upstream-blocks (list
            (nginx-upstream-configuration
              (name "radicale")
              (servers (list "127.0.0.1:5232")))))
          (server-blocks (list
            (nginx-server-configuration
              (ssl-certificate "/etc/letsencrypt/live/brielmaier.net/fullchain.pem")
              (ssl-certificate-key "/etc/letsencrypt/live/brielmaier.net/privkey.pem")
              (listen '("443 ssl http2")) ; no need for 80, we redirect via default-location
              (server-name '("brielmaier.net"))
              (root "/srv/http/brielmaier.net")
              (locations (list
                (nginx-location-configuration
                  (uri "/radicale/")
                  (body (list
                    "proxy_pass http://radicale/;"
                    "proxy_set_header X-Script-Name /radicale;"
                    "proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;"
                    "proxy_set_header Host $http_host;"
                    "proxy_pass_header Authorization;"))))))))
          (shepherd-requirement '(radicale))))
      (service openssh-service-type
         (openssh-configuration
           (authorized-keys
             `(("jonathan" ,(local-file "keys/x270.pub")
                           ,(local-file "keys/3700X.pub"))))
           (password-authentication? #f)
           (port-number 2123)))
      (service radicale-service-type)
      (modify-services %base-services
       (guix-service-type config =>
          (guix-configuration
            (inherit config)
            (extra-options '("--cores=6" "--max-jobs=1"))))))))

